- 2017.02.11
  * Experimenting with removing the NoF category, and simply maximizing accuracy over fish categories. Observations:
      * Getting as high as 96% validation accuracy.
      * Rotational data augmentation appears to be unimportant
      * Translational data augmentation appears to be VERY important (training accuracy goes to 100% and it memorizes the training set. Validation accuracy maxes out at 88%)
  * IDEA: sweep over entire image and make classification predictions for each patch (classifier only trained on fish, skipping NoF category.) Then summarize the picture by taking the max over each prediction category; similar to max pooling. Then train a second neural net to make a final classification based on this summary.
  * Interesting: "You can select up to 2 submissions to be used to calculate your final leaderboard score. If 2 submissions are not selected, they will be chosen based on your best submission scores on the public leaderboard."
  * Made first submissions to leaderboard.
      * Using silly approach of patch from upper left corner results in a score of 1.51475. Interestingly, this is much higher error than I get when using randomly held out data from the training set.
      * Using a uniform constant guess results in score of 2.07944
      * Using a constant guess weighted by unconditioned probabilities of the classes gets a score of 1.64611. The fact that this does better than the uniform distribution suggests that there is some relation between the class distributions of the initial training and test sets.
  * Experimenting with fish/non-fish detector that draws from same set of images, rather than NoF.
- 2017.02.05
  * Implemented some basic code that extracts random patches and applies ImageDataGenerator to them. First was experimenting with building an auto-encoder, but after conversation last night with Jad, probably will abandon this and go directly for a classifier.
  * Copied the Kaggle fish data to the P2 instance. Note that the default amount of disk on the P2 instance doesn't quite fit the entire data set if you try to un-tar it. So I ended up compressing and uncompressing each subdirectory individually, which worked fine.
  * Implemented code to create patches from the Nature Conservancy Fisheries data plus the shared bounding box info. Has option to automatically create square patches with fish centered in them so that there will be a common height/width ratio to feed to the neural net.
      * Considering having the patches be even larger so that they are unaffected by rotation (nothing missing at edges of background)  
  * Committed [semi-working version](https://gitlab.com/jadnohra/p77/blob/master/code/tom/kaggle.py) of pipeline.
      * Initial results look reasonable - getting about 90% accuracy so far on validation data (need to account for the fact that some of the categories are far more represented than others, however.)
      * Hilariously enough, it appears that it is possible to get about 70% accuracy on held out examples using just patches taken from the upper left corners.
- 2017.02.02
  * Looking into image preprocessing and augmentation, and found [this](https://keras.io/preprocessing/image/)!
- 2017.01.28
  * Have a GPU instance on AWS, getting CUDA and gpu-endabled tensorflow/keras working...
      * [Installing CUDA](http://docs.nvidia.com/cuda/cuda-installation-guide-linux/#axzz4WNL7OgLr)
  * Got it working! Adding install notes to README
- 2017.01.24
  * Getting up to speed on AWS. After a couple hours of confusion, it's actually not that bad. Reading about their GPU options:
      * [Quick Comparison of TensorFlow GPU Performance on AWS P2 and G2 Instances](http://www.bitfusion.io/2016/11/03/quick-comparison-of-tensorflow-gpu-performance-on-aws-p2-and-g2-instances/) 
  * Apparently I have to make a special request to customer support to enable my account for access to GPU instances. Yuck. Hopefully they fast in responding.

- 2017.01.15
  * Got LSTM code working in keras. However, I'm a bit confused by their concept of "batch size" w.r.t. RNNs, so investigating further.
      * http://stats.stackexchange.com/questions/153531/what-is-batch-size-in-neural-network
      * http://philipperemy.github.io/keras-stateful-lstm/
      * https://github.com/fchollet/keras/blob/master/examples/stateful_lstm.py
  * Reading source code for [recurrent.py](https://github.com/fchollet/keras/blob/master/keras/layers/recurrent.py)
  * Makes more sense now. Implemented the pathological adding problem to make sure LSTM is working as I expect:

```python
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.layers import LSTM, GRU
import numpy as np
import random
import sys
import math

maxlen = 20
indim = 2
hiddim = 10
outdim = 1
tot = 500
epochs = 100

X = np.zeros((tot, maxlen, indim), dtype=np.float)
y = np.zeros((tot, outdim), dtype=np.float)

for i in range(tot):
    t1 = int(random.random()*maxlen)
    t2 = int(random.random()*maxlen)
    while t1 == t2:
        t2 = int(random.random()*maxlen)
    added = 0
    for t in range(maxlen):
        val = random.random()
        X[i, t, 0] = val
        if t == t1 or t == t2:
            added += val
            X[i, t, 1] = 1.0
    y[i, 0] = added

#print(X)
#print(y)

print('Build model...')
model = Sequential()
model.add(LSTM(hiddim, input_shape=(maxlen, indim)))
model.add(Dense(outdim, init='normal'))

model.compile(loss='mean_squared_error', optimizer='adam')
model.fit(X, y, batch_size=1, nb_epoch=epochs)
```

- 2017.01.10
  * Further playing around with image processing using scipy. Added more example code below.

- 2017.01.09
  * Installing and experimenting with [Pillow](https://pypi.python.org/pypi/Pillow/2.2.1), an image processing library for Python
  * Looks like [scipy](http://www.scipy-lectures.org/advanced/image_processing/) might have some similar capabilities

```python
#sample code
from scipy import misc
from scipy.ndimage import rotate
import matplotlib.pyplot as plt
img = misc.imread('img_01756.jpg') #return a numpy array of shape 670, 1192, 3
rot = rotate(img, 30, reshape=False)
plt.imshow(rot)
plt.show()
smaller_dim = misc.imresize(img, 0.5)
plt.imshow(smaller_dim)
plt.show()
blackAndWhite = misc.imread('img_01756.jpg', mode='L')
plt.imshow(blackAndWhite, cmap='gray')
plt.show()
```

- 2017.01.07
  * Reading about how to format datasets in Keras
- 2017.01.01
  * Reading more about Keras. Becoming convinced we should use that rather than TensorFlow. Much more elegant and well designed.
  * Got the Keras [ResNet](https://github.com/raghakot/keras-resnet) example Jad found up and running. Ran into no problems.
- 2016.12.31
  * Studying TensorFlow [ResNet example](https://github.com/tensorflow/models/tree/master/resnet)
  * Getting [Cifar10 tutorial](https://www.tensorflow.org/tutorials/deep_cnn/) running
      * Ran into multiple bugs with this, and feeling frustrated by other aspects of TensorFlow (will log later)
  * Experimenting with [Keras](https://keras.io/)
- 2016.12.30
  * Studying TensorFlow (similar to prior entry)
  * Brushing up on [convolutional nets](http://cs231n.github.io/convolutional-networks/)
- 2016.12.28
  * Studying TensorFlow
      * Trying to understand better what does and doesn't have to occur in a Session
      * Playing around with tensor transformation operations (e.g. reshape)
      * Looking at source code of existing solutions for implementation patterns
      * Going over [ResNet code](https://github.com/tensorflow/models/tree/master/resnet) from official TensorFlow repository
      * Also reading through [this](https://www.tensorflow.org/tutorials/mnist/tf/) tutorial
  * Reading over Jad's notes/log