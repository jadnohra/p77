- A bit of research into classifier robustness and adversarial examples led me to create this presentation: [slides](./presi/jad/adversarial/presi.pdf), [script](./presi/jad/adversarial/script.pdf), [code](./presi/jad/adversarial/adversarial.py), [all](./presi/jad/adversarial)
- Reading Tom's [plan outline](https://docs.google.com/document/d/1jNmAjWTZcnMIiBpUoqKrO7vJDP13FrfwVMSxHfjdELQ/edit?usp=sharing)
 - Exploration
     - [Basic questions and answers](https://m.reddit.com/r/MachineLearning/comments/49hu9r/cnns_for_image_classification_with_nonsquared/?ref=readnext_6)  (square vs. non-square images, pretrained)
     - [Shark Tuna Fish Detector using Inception ResNet v2 Tensorflow Slim pretrained](https://github.com/bigsnarfdude/FsshFssh) (very early still not functional)
     - [TF-Slim is a lightweight library for defining, training and evaluating complex models in TensorFlow.] (https://github.com/tensorflow/tensorflow/tree/master/tensorflow/contrib/slim)
     - [Kaggle fish dataset discussion](https://www.kaggle.com/c/the-nature-conservancy-fisheries-monitoring/forums/t/25428/official-pre-trained-model-and-data-thread?forumMessageId=149298)
            - 'external data is allowed as long as it's posted on that thread, says kaggle admin'
        - External sets
            - [Labeled Fishes in the Wild](http://swfscdata.nmfs.noaa.gov/labeled-fishes-in-the-wild/)
            - [Fish Dataset](https://wiki.qut.edu.au/display/cyphy/Fish+Dataset)
            - [3D fish model generator](http://swordtail.tamu.edu/anyfish/Main_Page), [Morphometrics](http://swordtail.tamu.edu/anyfish/Modeling_body_and_fin_shape_(morphometrics))
        - Community data
            - [Competition fish bounding boxes](https://www.kaggle.com/c/the-nature-conservancy-fisheries-monitoring/forums/t/25902/complete-bounding-box-annotation) 
     - Distractions
            - [Youtube, Convolutional nets in 10 minutes](https://www.youtube.com/watch?v=LodC7Zm3X8Q) 
 - Conclusions
        - Probably I will try to look into finding a good pre-trained model, and learning how to fine tune it, using the kaggle training set, or using a public data set.  
        - Object Detection for finding fish
            - [Intro: Object Recognition with Convolutional Neural Networks in the Keras Deep Learning Library](http://machinelearningmastery.com/object-recognition-convolutional-neural-networks-keras-deep-learning-library/) 
            - [Deep Neural Networks for Object Detection](https://pdfs.semanticscholar.org/713f/73ce5c3013d9fb796c21b981dc6629af0bd5.pdf)
            - [Awesome Deep Vision / Object Detection](https://github.com/kjw0612/awesome-deep-vision#object-detection)
        - Object Detection with ResNet
            - That's 'soa', and used for the winner in kaggle-whale
            - ["Deep Residual Learning for Image Recognition"](http://arxiv.org/abs/1512.03385)
            - [ResNet](https://github.com/KaimingHe/deep-residual-networks)
            - [ResNet/tf](https://github.com/ry/tensorflow-resnet)
            - [ResNet/tf](https://github.com/ppwwyyxx/tensorpack/tree/master/examples/ResNet)
            - [ResNet/Krs](https://github.com/raghakot/keras-resnet)
            - [Example of a pre-trained kaggle for another competitition](https://www.kaggle.com/c/state-farm-distracted-driver-detection/forums/t/20747/simple-lb-0-23800-solution-keras-vgg-16-pretrained)
            - [Tip on using resnet for detection vs. classification](https://groups.google.com/forum/#!topic/torch7/jEd4FGyYc6w)
            - [Trained ResNet Torch models, with tips on tuning](https://github.com/facebook/fb.resnet.torch/tree/master/pretrained)[2]
            - [Keras/PreTrained, official docs](https://keras.io/applications) [1]
            - [Fish on imagenet](http://image-net.org/synset?wnid=n02512053) [3], [More inet fish (e.g per species)](http://image-net.org/search?q=fish)
            - [Reading the imagenet results and comments is useful and supports some of our intuitions, especially about ensemble](http://image-net.org/challenges/LSVRC/2016/results#team)
            - [Example of object detection dataset](http://image-net.org/challenges/LSVRC/2015/ui/det.html)
    - It looks like we should start with [1],[2],[3] using: 
        
            keras.applications.resnet50.ResNet50(include_top=True, weights='imagenet', input_tensor=None, input_shape=None)

    - We will first try labelling, without localisation (detection) (the result which took one day is [Res-1]), then we will add a sliding window or another approach
        - [Heatmap for detection](https://github.com/heuritech/convnets-keras)
        - [Question with answers about localisation techniques](https://www.researchgate.net/deref/http%3A%2F%2Fcs231n.stanford.edu%2Fsyllabus.html)
        - [Nice sliding window tutorial](http://www.pyimagesearch.com/2015/03/23/sliding-windows-for-object-detection-with-python-and-opencv/)
        - [Face detection using sliding window](http://cs.brown.edu/courses/cs143/proj4/)
        - [Reddit-Keras LSTM limitations](https://www.reddit.com/r/MachineLearning/comments/3dqdqr/keras_lstm_limitations/)
        - [Reddit-LSTM/RNN Network vs. Sliding Window with Feedforward Network](https://www.reddit.com/r/MachineLearning/comments/4uv9rx/lstmrnn_network_vs_sliding_window_with/)
            
                    I agree that it's hard to grasp why LSTM layers perform better than a sliding window, but it seems like it does in many experiments.
        - Sliding window results are [Res-2]

- Results
    1. [Notebook: ResNet untuned, whole-image classif on kgl-fish tests](./fish/scripts/ResNet_test1.ipynb.json), [Pdf](./fish/scripts/ResNet_test1.pdf) [Res-1]
    2. [Added sliging window and image annotation to above](./fish/scripts/ResNet_untuned_sliding_1) [Res-2]
        - Given this is fully generic, it is not too bad even if it's usable
        - <img src=../media/annot-YFT-img_00217.jpg height=256 /> 
        - <img src=../media/annot-BET-img_02736.jpg height=256 /> 
        - <img src=../media/annot-elephant.png height=256 /> 
        - Note that the labels for ResNet untuned come from here: TODO, and do not include all of imagenet labels, so only a subset of 'fish' is there.

- Thinking
    - [More fine-tuning ResNet detail](https://github.com/facebook/fb.resnet.torch/tree/master/pretrained) 
    - [A good kgl/fish discussion about a currently top 5% shared code](https://www.kaggle.com/c/the-nature-conservancy-fisheries-monitoring/forums/t/26202/using-keras-tensorflow-to-solve-ncfm-leadboard-top-5), [Github](https://github.com/pengpaiSH/Kaggle_NCFM)
    - [A complete keras finetuning tutorial](https://blog.keras.io/building-powerful-image-classification-models-using-very-little-data.html)
    - More finetuning discussions [here](https://github.com/fchollet/keras/issues/871) and [here]( https://gist.github.com/baraldilorenzo/07d7802847aaad0a35d3)
    - As a first thing to do, a good exercise is to finetune ResNet-imagenet on one or two classes we care about on imagenet and test this.

- Done: Load and display manual bounding boxes (generated by sloth tool)
    - I wrote a [jupyter notebook that does this](./fish/scripts/KglFish_disp_bbox.ipynb.json). Unfortunately, it seems that the data provided by [this guy](https://www.kaggle.com/c/the-nature-conservancy-fisheries-monitoring/forums/t/25902/complete-bounding-box-annotation) is garbage (also mentioned by some users). As a test, run the notebook and look at the debug info and image at the end. The data is all loaded and assigned correctly, but it does not correspond to the fish and it does not look like mixup of coordinates or similar.
    - <img src=../media/KglFish_disp_bbox.jpg height=128 />
    - I will have to test on a self-annotated image and if indeed there is not bug, we will have to annotate manually....
    - ~~If Tom has time, he could get the notebook and try to check my theory that the data is simply garbage. To do this he needs to make sure the paths are locally correct for the variables: g_train_path (directory where directories like YFT/*.jpg are), g_bbox_path (directory where files like yft_labels.json are)~~
    - Strike that, it was a bug in my rendering code. Works fine now!
- Done:
    - Like for drawing kgl-fish bounding boxes, I did the same for the fish-in-the-wild (FINW) dataset
- Done:
    - After strenuous work, I have code on my aws instance that almost-automatically (bar git ssh passphrase) upon saving a notebook from jupyter's web interface  
        1. cleans the output from notebooks (otherwise they are very large since the images and results are embedded)
        2. copies the cleaned notebooks to a git repo clone on the aws instance
        3. commits the changes to gitlab, the comment is 'commit_jupyter'
    - Restructured the repo such that such code lives in [code/jad](./code/jad). Tom's can live in [code/tom](./code/tom). Collaboration code in [code/collab](./code/collab)

- Practicing [A complete keras finetuning tutorial](https://blog.keras.io/building-powerful-image-classification-models-using-very-little-data.html)
    - As planned, worked out part of the tutorial. It took some time because I wanted to setup a p2 instance instead of a g2 instance and that meant moving to another region/zone.
    - The notebook is keras_tut1_catdog.ipynb, I trained the simple network on cats and dogs. It took 20 secs per epoch (both p2 and g2 :( ) (50 secs for cpu on medium instance)
    - next is continuing the tutorial
- Practicing
    - Fought keras/vgg16 img_order and backend different issues
- Gave up on trying to load/convert VGG theano weights into tensorflow. Instead I now start keras with the backend of the file (by editing ~/.keras/keras.json before starting a notebook)
- Continued the catdog tuning tutorial. There is a problem: training the networks 'top' does not converge and has the same accuracy and loss for all 50 epochs.
- Done:
    - The problem with the network not converging is either me using the wrong image dimensions, or the wrong ordering in keras.json (TBD).
    - After changing those to 155 and th, the tutorial worked fine with >90% accuracy. The code file is [keras_tut1_catdog_bottleneck](./code/jad/keras_tut1_catdog_bottleneck.ipynb)
    - Next is to finish off the tutorial and fine tune the top layer even more, then create slightly modular code to be used for kgl-fish.
- Preparing:
    - Before fine tuning the top layer even more, I decided to modularize the code while also testing a new variant of it: cat-dog-fish (using 'Labeled Fishes in the Wild')
    - In [keras_catdogfish](./code/jad/keras_catdogfish.ipynb), I now have a really nice single function that supports all ways of creating a vgg16 for fine tuning or for the 'default' imagenet weights:

        > f_make_vgg16_model(input=None, bottleneck=None, top=None, bottleneck_weights=None, top_weights=None, weights=None, verbose=True)
- Preparing:
    - Continued working on the above, I have a a working 'retop' function that is easy to use. I suspect the labels provided while 'retopping' might be wrong, and are right in the tutorial only because it happens that the number of images fits the batch size. This has to be investigated next.
    - After that, retop can be made to take an arbitrary number of classes and is ready to be used on 'fish in the wild' as cat/dog/fish
    - Note that it might make sense to find image transformations that emulate the kinds of camera angles (and even backgrounds) to run while learning 'fish' to be used with kgl-fish 
- Preparing:
    - Continuing the above, I decided to stop trying to make the vgg16 adaptation work around the difficulty of finding the right labels when the number of files is not the same between classes.
    - By this, I bypassed the tutorial's clever cashing of bottleneck layer output. In any case, it does not generalize well with augmentation.
    - With these decisions, I was able to write a simpler version that easily generalizes and tunes for any number of classes. I put it in [keras_catdogfish2](./code/jad/keras_catdogfish2.ipynb)
- Minor result:
    - After doing the above and running with :
    
        > f_vgg16_make_adapted( ..., img_sz=80, n_tv_samples=(2000, 800), n_epochs=15, batch_sz=32)
        
    I get 0.85 validation accuracy, this is an example of running with the resulting weights:
    
    >   
        train/dogs/dog554.jpg  -> dogs (0.00 1.00 0.00)
        train/cats/cat443.jpg  -> cats (1.00 0.00 0.00)
        train/fish/DSCN4810.jpg  -> fish (0.00 0.00 1.00)
        train/cats/cat344.jpg  -> cats (1.00 0.00 0.00)
        train/fish/DSCN4309_1.JPG  -> fish (0.00 0.00 1.00)
        train/dogs/dog284.jpg  -> dogs (0.00 1.00 0.00)
        train/cats/cat501.jpg  -> cats (1.00 0.00 0.00)
        train/dogs/dog58.jpg  -> dogs (0.00 1.00 0.00)
        train/fish/Set2_DSCN3214.JPG_0.jpg  -> fish (0.00 0.00 1.00)
        train/dogs/dog593.jpg  -> cats (1.00 0.00 0.00)
        validation/cats/cat2017.jpg  -> cats (1.00 0.00 0.00)
        validation/fish/DSCN4825.jpg  -> fish (0.00 0.00 1.00)
        validation/cats/cat2148.jpg  -> dogs (0.00 1.00 0.00)
        validation/dogs/dog2185.jpg  -> dogs (0.00 1.00 0.00)
        validation/dogs/dog2346.jpg  -> dogs (0.00 1.00 0.00)
        validation/cats/cat2122.jpg  -> cats (1.00 0.00 0.00)
        validation/cats/cat2253.jpg  -> cats (1.00 0.00 0.00)
        validation/dogs/dog2266.jpg  -> dogs (0.00 1.00 0.00)
        validation/fish/DSCN4360_1.JPG  -> fish (0.00 0.00 1.00)
        validation/cats/cat2133.jpg  -> cats (1.00 0.00 0.00)
        
- Note: Finally was able to properly make keras+theano use the GPU on a p2 instance. 10x faster that cpu when training. Ran into a bug that caused theano to crash on initialization because it seems, both it and tensorflow were trying to use the gpu and there seems to be a problem with that in some versions. The cuda/cudnn message is similar to [this](http://stackoverflow.com/questions/39550258/keras-errors-with-theano-tensowflow-backend), but already happens on init. The solution was to remove the tensorflow-gpu package for now.
- Training curves: Added a small tool to plot the training history as returned by keras. This hisotry (dictionary) can be simply printed as a string to a file, the tool can turn such a file into a plot (e.g in a jupyter notebook). The tool is [here](./code/jad/trainhist_tools.ipynb). This is an example plot from the previous experiment: - <img src=../media/histfile_plot_example.png height=160 /> 
- Preparing:
    - Wrote a tool that turns bounding box information into cut images of fish from the original image. This provides a training directory by class of all actual fish in the images. The tool is [kglfish_bbox_cut.py](./code/jad/kglfish_bbox_cut.py)
    - Added the ability to set a regular patch size, this is handled robustly such that no rectangles end up being illegal, while trying to keep the rectangles intersection as much as possible with the annotated label rectangle
    - Added the ability to also generate patches at random from the 'NoF' class even though it has no bounding box annotations.
- Trying
    - Using the above generated image patches (100x100) in combination with the keras vgg16 'retop' code developed previously to see what kind of training behavior we get.
    - Training fails.