### Data paths, classes
import sys
g_arg_src = sys.argv[sys.argv.index('-src')+1]
g_arg_boxes = sys.argv[sys.argv.index('-boxes')+1]
g_arg_dest = sys.argv[sys.argv.index('-dest')+1]
g_arg_flatten = '-flatten' in sys.argv
g_arg_dry = '-dry' in sys.argv
g_arg_no_overwrite = '-no_overwrite' in sys.argv
g_arg_patch_sz = [int(x) for x in sys.argv[sys.argv.index('-patch')+1].split(',')] if '-patch' in sys.argv else None
g_arg_no_empty_class = '-no_empty' in sys.argv
g_arg_empty_mul = int(sys.argv[sys.argv.index('-empty_mul')+1])

import os, os.path
g_files_ = lambda x: [f for f in os.listdir(x) if os.path.isfile(os.path.join(x, f))]
g_dirs_ = lambda x: [f for f in os.listdir(x) if os.path.isdir(os.path.join(x, f))]
g_files = lambda x: [os.path.join(x, f) for f in os.listdir(x) if os.path.isfile(os.path.join(x, f))]
g_dirs = lambda x: [os.path.join(x, f) for f in os.listdir(x) if os.path.isdir(os.path.join(x, f))]
g_train_path = g_arg_src
g_train_classes = g_dirs_(g_train_path)
g_n_classes = len(g_train_classes)
g_cls_i = lambda x: [y.lower() for y in g_train_classes].index(x.lower())
g_train_files_ = [g_files_(x) for x in g_dirs(g_train_path)]
g_train_files = []
g_train_paths = []
for cls_i in range(len(g_train_classes)):
    files = [os.path.join(g_train_classes[cls_i], x) for x in g_train_files_[cls_i]]
    g_train_files.append(files)
    g_train_paths.append([os.path.join(g_train_path, x) for x in files])
print 'Classes', g_train_classes

### Image

import scipy.ndimage
import scipy.misc
import matplotlib.image

def fimg_load_ar(img_path):
    return matplotlib.image.imread(img_path)
def fimg_ar_size(img_ar):
    return (len(img_ar[0]) if img_ar is not None and len(img_ar) else 0.0, len(img_ar) if img_ar is not None else 0.0)
def fimg_ar_racoon():
    return scipy.misc.face()
def fimg_ar_resize(img_ar, res):
    return scipy.ndimage.zoom(img_ar, (res[0],res[1],1))
def fimg_ar_clip_rect(img_ar, rect):
    pts = [ (rect[0], rect[1]), (rect[0]+rect[2], rect[1]+rect[3]) ]
    border_pts = [ (0,0), fimg_ar_size(img_ar) ]
    npts = [ [max(pts[0][x], border_pts[0][x]) for x in [0,1]], [min(pts[1][x], border_pts[1][x]) for x in [0,1]] ]
    nrect = [ npts[0][0], npts[0][1], npts[1][0]-npts[0][0], npts[1][1]-npts[0][1] ]
    return nrect

### Bbox data
import json
import math
import sys
import random

def progress_100(prefix, i, n, endl=True):
    ii = int(1.0 + float(i)/float(n)*float(100))
    sys.stdout.write('\r')
    sys.stdout.write('{}[{}{}] {}%'.format(prefix, '='*(ii/5), ' '*(20-ii/5), ii) )
    sys.stdout.flush()
    if endl and i+1 >= n:
        sys.stdout.write('\n')

g_bbox_path = g_arg_boxes
g_bbox_class_files = ['{}_labels.json'.format(x.lower()) for x in g_train_classes]
def fbbox_load_file_rects(train_classes = g_train_classes, 
                        train_files = g_train_files, 
                        bbox_class_files = g_bbox_class_files,
                        dbg_by_file = False,
                        fix_rects = True, patch_sz = g_arg_patch_sz, 
                        train_paths = g_train_paths,
                        no_class = 'NoF', no_class_mul = g_arg_empty_mul):
    def move_child_in(parent, child):
            mv = [(parent[0]+parent[2]) - (child[0]+child[2]), (parent[1]+parent[3]) - (child[1]+child[3])]
            mv = [min(0, x) for x in mv]
            return [child[0]+mv[0], child[1]+mv[1], child[2], child[3]]
    def rect_to_patches(img_ar, rect, patch_sz):
        cnts = [int(math.ceil(x)) for x in [float(rect[2])/float(patch_sz[0]), float(rect[3])/float(patch_sz[1])]]
        prects = set()
        for ii in range(cnts[0]):
            for jj in range(cnts[1]):
                prect = [ rect[0]+ii*patch_sz[0], rect[1]+jj*patch_sz[1], patch_sz[0], patch_sz[1]]
                prect = move_child_in(rect, prect)
                prect = move_child_in([0,0]+list(fimg_ar_size(img_ar)), prect)
                prect = fimg_ar_clip_rect(img_ar, prect)
                prects.add(tuple(prect))
        return list(prects)
    rect_cnt = 0
    iif = lambda x: int(float(x))
    bbox_files = []
    print 'Loading rectangles ...'
    for cls_i in range(len(train_classes)):
        cls = train_classes[cls_i]
        cls_files = [os.path.basename(x) for x in train_files[cls_i]]
        bbox_path = os.path.join(g_bbox_path, bbox_class_files[cls_i])
        cls_bbox_files = [[] for x in cls_files]
        if no_class is not None and cls == no_class and patch_sz is not None:
            ndict_i = 0; ndict = len(cls_files);
            for findex, fname in enumerate(cls_files):
                progress_100(' {} (auto-gen): '.format(no_class), ndict_i, ndict); ndict_i = ndict_i+1;
                img_ar = fimg_load_ar(train_paths[cls_i][findex])
                img_rect = [0,0] + list(fimg_ar_size(img_ar))
                for ii in range(no_class_mul):
                    prect = [random.randint(0, img_rect[2+x]-1-patch_sz[x]) for x in [0,1]] + list(patch_sz)
                    prect = move_child_in(img_rect, prect)
                    cls_bbox_files[findex].append(prect)
                    rect_cnt = rect_cnt+1
        else:
            if (os.path.exists(bbox_path)):
                #print cls, bbox_path
                with open(bbox_path) as fi:
                    bbox_dict = json.loads(fi.read())
                    ndict_i = 0; ndict = len(bbox_dict);
                    for file_info in bbox_dict:
                        progress_100(' {}: '.format(cls), ndict_i, ndict); ndict_i = ndict_i+1;
                        fname = os.path.basename(file_info['filename'])
                        if fname in cls_files:
                            findex = cls_files.index(fname)
                            img_ar = None
                            if fix_rects or patch_sz is not None:
                                img_ar = fimg_load_ar(train_paths[cls_i][findex])
                            file_annots = file_info['annotations']
                            for annot in file_annots:
                                if annot['class'] == 'rect':
                                    rect = [iif(annot['x']), iif(annot['y']), iif(annot['width']), iif(annot['height'])]
                                    if fix_rects or patch_sz is not None:
                                        rect = fimg_ar_clip_rect(img_ar, rect)
                                    if patch_sz is not None:
                                        patch_rects = rect_to_patches(img_ar, rect, patch_sz)
                                        cls_bbox_files[findex].extend(patch_rects)
                                        rect_cnt = rect_cnt+len(patch_rects)
                                    else:
                                        cls_bbox_files[findex].append(rect)
                                        rect_cnt = rect_cnt+1
                                else:
                                    print 'Warning, annot is not a rect'
                            if dbg_by_file and len(file_annots):
                                print fname, cls_bbox_files[findex]
                        else:
                            #print cls_files
                            print 'Warning: not found file {} for bbox'.format(fname)
            else:
                print 'Warning: not found bbox for class:', cls
        bbox_files.append(cls_bbox_files)
    return (bbox_files, rect_cnt)

g_bbox_file_rects, g_bbox_rect_cnt = fbbox_load_file_rects(patch_sz = g_arg_patch_sz, no_class = None if g_arg_no_empty_class else 'NoF')
print 'Loaded {} bounding boxes'.format(g_bbox_rect_cnt)

### Image cut
import random
    
def fimg_ar_cut_bbox(img_ar, rect):
    rect = fimg_ar_clip_rect(img_ar, rect)
    return img_ar[rect[1] : rect[1]+rect[3], rect[0] : rect[0]+rect[2]]
    
def fimg_ar_cut_bboxes(img_ar, rects):
    return [fimg_ar_cut_bbox(img_ar, rect) for rect in rects]

def fimg_cut_bboxes_ar(cls_i, img_i):
    img_ar = fimg_load_ar(g_train_paths[cls_i][img_i])
    rects = g_bbox_file_rects[cls_i][img_i]
    return fimg_ar_cut_bboxes(img_ar, rects)

def fimg_cut_bboxes_ar_rand():
    cls_i = random.randint(0, len(g_train_classes)-1)
    img_i = random.randint(0, len(g_train_paths[cls_i])-1)
    return fimg_cut_bboxes_ar(cls_i, img_i) 

### export
import os

dest_dir = g_arg_dest
n_files = []
print 'Writing images ...'
if g_arg_dry and g_arg_flatten:
    print 'destination directory: [{}]'.format(dest_dir)
for cls_i in range(len(g_train_classes)):
    n_img = len(g_train_paths[cls_i])
    cls_dest_dir = dest_dir if g_arg_flatten else os.path.join(dest_dir, g_train_classes[cls_i])
    cls_n_files = 0
    if g_arg_dry and g_arg_flatten == False:
        print 'destination directory: [{}]'.format(cls_dest_dir)
    if g_arg_dry == False and g_arg_flatten == False:
        if os.path.exists(cls_dest_dir) == False:
            os.mkdir(cls_dest_dir)
    for img_i in range(n_img):
        cut_imgs_ar = fimg_cut_bboxes_ar(cls_i, img_i)
        cls_n_files = cls_n_files + len(cut_imgs_ar)
        if len(cut_imgs_ar):
            src_fname = g_train_files_[cls_i][img_i]
            src_splt = os.path.splitext(src_fname)
            tgt_fnames = ['{}-{}{}'.format(src_splt[0], x, src_splt[1]) for x in range(len(cut_imgs_ar))]
            if g_arg_dry:
                print '[{}] -> [{}]'.format(src_fname, ', '.join(tgt_fnames))
            else:
                for ci in range(len(cut_imgs_ar)):
                    tgt_path = os.path.join(cls_dest_dir, tgt_fnames[ci])
                    do_save = True
                    if g_arg_no_overwrite:
                        do_save = os.path.exists(tgt_path)
                    else:
                        if os.path.exists(tgt_path):
                            os.remove(tgt_path)
                    if do_save:
                        matplotlib.image.imsave( tgt_path, cut_imgs_ar[ci] )
                progress_100(' ({}/{}) {}: '.format(cls_i+1, len(g_train_classes), g_train_classes[cls_i]), img_i, n_img)
    n_files.append(cls_n_files)
    print ' ({} files)'.format(cls_n_files)
print '[{}] files'.format(', '.join([str(x) for x in n_files]))