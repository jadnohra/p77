import os.path
import shutil
import filecmp

'''
# Put this in your jupyter config

#
# Custom Hooks
#
import os
g_shell_path = '/home/admin/Notebooks/shell'
g_hook_pre_save_path = os.path.join(g_shell_path, 'save_hooks.py')
g_hook_pre_save_func = 'exec_hook_pre_save'
g_hook_post_save_path = os.path.join(g_shell_path, 'save_hooks.py')
g_hook_post_save_func = 'exec_hook_post_save'
g_hook_verbose = False

def hook_pre_save(model, path, contents_manager):
 hook_path, hook_name = g_hook_pre_save_path, g_hook_pre_save_func
 if os.path.isfile(hook_path):
  if g_hook_verbose:
   print ' > hooking pre_save:', hook_path,
  exec_dict = {}
  execfile(hook_path, exec_dict)
  if hook_name in exec_dict:
   if g_hook_verbose:
    print hook_name, '...'
   exec_dict[hook_name](model, path, contents_manager)
  if g_hook_verbose:
   print ' > done'
def hook_post_save(model, os_path, contents_manager):
 hook_path, hook_name = g_hook_post_save_path, g_hook_post_save_func
 if os.path.isfile(hook_path):
  if g_hook_verbose:
   print ' > hooking post_save:', hook_path,
  exec_dict = {}
  execfile(hook_path, exec_dict)
  if hook_name in exec_dict:
   if g_hook_verbose:
    print hook_name, '...'
   exec_dict[hook_name](model, os_path, contents_manager)
  if g_hook_verbose:
   print ' > done'

c.FileContentsManager.pre_save_hook = hook_pre_save
c.FileContentsManager.post_save_hook = hook_post_save
'''

## git command monitor
import sys, subprocess, os, time, fcntl
def run_git_command(pargs, cwd, pass_pwd=True):
    os.environ['GIT_ASKPASS'] = 'true'
    os.environ['GIT_TERMINAL_PROMPT']='0'
    p = subprocess.Popen(' '.join(pargs), stdout=subprocess.PIPE, stderr=subprocess.STDOUT, stdin=subprocess.PIPE, cwd=cwd, bufsize=1, shell=True)
    #print os.path.expandvars('$AUTOCOMMIT_PASSPHRASE')
    #p.stdin.write(os.path.expandvars('$AUTOCOMMIT_PASSPHRASE')); p.stdin.write('\n'); p.stdin.close();
    fcntl.fcntl(p.stdout, fcntl.F_SETFL, fcntl.fcntl(p.stdout, fcntl.F_GETFL) | os.O_NONBLOCK)
    start_time = time.time()
    while p.poll() is None:
        try:
            while True:
                print '[{}]'.format(p.stdout.readline().strip())
        except:
            pass
        #print int(time.time()-start_time)
        time.sleep(1)
    try:
        print '[{}]'.format(p.stdout.read().strip())
    except:
        pass
    return (p.poll() == 0)
##

def extract_hook_cell_info(model):
    ret = {'repo_path':'', 'drop_output':False}
    if model['type'] == 'notebook' and len(model['content']['cells']) and model['content']['cells'][0]['cell_type'] == 'code':
        cfg_cell = model['content']['cells'][0]
        repo_dir = ''
        for l in [x[2:].strip() for x in cfg_cell['source'].split('\n') if x.startswith('##')]:
            words = l.split(' ')
            if words[0].lower() == 'repo_path':
                repo_path = ' '.join(words[1:])
                if len(repo_path) and os.path.split(repo_path)[1].endswith('.ipynb') == False:
                    repo_path = os.path.join(repo_path, os.path.basename(model.get('path', '')))
                    repo_path = repo_path.replace('~', os.path.expanduser('~'))
                ret['repo_path'] = repo_path
            if words[0].lower() == 'drop_output':
                ret['drop_output'] = True
    return ret

def clean_code_output(model):
    if model['type'] == 'notebook':
        for cell in [x for x in model['content']['cells'] if x['cell_type'] == 'code']:
            cell['outputs'] = []
            cell['execution_count'] = None

def copy_commit_file(src_path, dest_path):
    if os.path.isfile(dest_path) == False or filecmp.cmp(src_path, dest_path) == False:
        print ' : copy [{}] -> [{}]'.format(src_path, dest_path)
        shutil.copyfile(src_path, dest_path)
        if False: # This is not working due to ssh passphrases
            cwd = os.path.split(dest_path)[0]
            success = all(run_git_command(x, cwd) for x in 
                          [ ['git', 'add', '.'], ['git', 'commit', '-m', '"auto-commit"'], ['git', 'push'] ])
            print ' : git push {}'.format('succeeded' if success else 'failed')
    else:
        print ' : Unchanged'
            
def exec_hook_pre_save(model, path, cmanager):
    hook_info = extract_hook_cell_info(model)
    if hook_info['drop_output']:
        clean_code_output(model)
    return


def exec_hook_post_save(model, os_path, cmanager):
    #print 'os_path', os_path, model['path']
    hook_info = extract_hook_cell_info(cmanager.get(model['path']))
    #print hook_info
    if len(hook_info['repo_path']):
        copy_commit_file(os_path, hook_info['repo_path'])
    