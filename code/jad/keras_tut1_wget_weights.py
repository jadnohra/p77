import os, sys, subprocess

def file_size(file_path):
    bytes = os.stat(file_path).st_size if os.path.isfile(file_path) else 0
    for x in ['bytes', 'KB', 'MB', 'GB', 'TB']:
        if bytes < 1024.0:
            return "%3.1f %s" % (bytes, x)
        bytes /= 1024.0

def google_wget_dload(file_id, file_out):
    args_1 = ['wget', '--save-cookies cookies.txt', '--keep-session-cookies', '--no-check-certificate', "'https://docs.google.com/uc?export=download&id={}'".format(file_id), '-O-']
    proc = subprocess.Popen(' '.join(args_1), stdout = subprocess.PIPE, stderr=subprocess.PIPE, shell = True)
    (out_1, err) = proc.communicate()
    #print out_1, err
    args_2 = ['sed', '-rn', "'s/.*confirm=([0-9A-Za-z_]+).*/Code: \\1\\n/p'"]
    proc = subprocess.Popen(' '.join(args_2), stdin = subprocess.PIPE, stdout = subprocess.PIPE, shell=True)
    (out_2, err) = proc.communicate(input=out_1)
    #print out_2, err
    args_3 = ['wget', '--load-cookies cookies.txt', '-O', "'{}'".format(file_out), "'https://docs.google.com/uc?export=download&id={}&confirm={}'".format(file_id, out_2.split(':')[1].strip())]
    #print ' '.join(args_3)
    subprocess.Popen(' '.join(args_3), shell=True)
    proc.communicate()

file_id = '0Bz7KyqmuGsilT0J5dmRCM0ROVHc' if (not '-i' in sys.argv) else sys.argv[sys.argv.index('-i')+1]
file_out = 'weights.h5' if (not '-o' in sys.argv) else sys.argv[sys.argv.index('-o')+1]
google_wget_dload(file_id, file_out)
print '[{}] : {}'.format(file_out, file_size(file_out))