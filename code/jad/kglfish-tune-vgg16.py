
# coding: utf-8

# In[ ]:

### automation
## repo_path ~/repos/p77/code/jad/
## drop_output
# % autosave 21600

import sys

if True:
    g_arg_epochs = int(sys.argv[sys.argv.index('-epochs')+1]) if '-epochs' in sys.argv else 2
    g_arg_batch = int(sys.argv[sys.argv.index('-batch')+1]) if '-batch' in sys.argv else 32
    g_arg_refine = '-refine' in sys.argv
    g_arg_no_cache = '-no_cache' in sys.argv


# In[ ]:

### Settings

### Data dir
import os.path

g_data_problem_name = 'kgl-fish-manual/cutpatch'; 
g_data_base_dir_options = ['~/ml_data', '~/Notebooks/data']
g_data_base_dir = None;
g_data_prebase_dir = None;
for x in g_data_base_dir_options:
    if os.path.exists(os.path.expanduser(x)):
        g_data_prebase_dir = os.path.expanduser(x)
        g_data_base_dir = os.path.join(os.path.expanduser(x), g_data_problem_name)
        print 'Data dir is: [{}]'.format(g_data_base_dir)

f_data = lambda x: os.path.join(g_data_base_dir, x)
def f_no_base(file): 
    return file[len(g_data_base_dir)+1:] if (g_data_base_dir in file) else (file[len(g_data_prebase_dir)+1:] if g_data_prebase_dir in file else file)


# In[ ]:

### Utility
import os, os.path, time

def file_size(file_path):
    bytes = os.stat(file_path).st_size if os.path.isfile(file_path) else 0
    for x in ['bytes', 'KB', 'MB', 'GB', 'TB']:
        if bytes < 1024.0:
            return "%3.1f %s" % (bytes, x)
        bytes /= 1024.0
        
def f_explode(obj, n):
    if isinstance(obj, (list, tuple)):
        return obj if len(obj) == n else obj + [obj[-1]]*(n-len(obj))
    else:
        return [obj]*n

def f_aget(lst, i, dflt=None):
    return lst[i] if len(lst) > i else dflt
    
def f_start_timer(descr = "timing ..."):
    if descr is not None:
        print descr
    return time.time()
def f_end_timer(timer, descr = 'elpased'):
    print("{} [{:.2f} sec.]".format(descr, float(time.time() - timer)))


# In[ ]:

### Data
import os, os.path, random

f_fnames = lambda x: [f for f in os.listdir(x) if os.path.isfile(os.path.join(x, f))]
f_dirnames = lambda x: [f for f in os.listdir(x) if os.path.isdir(os.path.join(x, f))]
f_files = lambda x: [os.path.join(x, f) for f in os.listdir(x) if os.path.isfile(os.path.join(x, f))]
f_dirs = lambda x: [os.path.join(x, f) for f in os.listdir(x) if os.path.isdir(os.path.join(x, f))]

f_cls_index = lambda data,x: [y.lower() for y in data['classes']].index(x.lower())

def f_data_from_dir(data_dir, excl_cls_names = None):
    def fill_cls_files(data, key):
        data[key]['cls_fnames'] = [f_fnames(os.path.join(data[key]['dir'], x)) if data[key]['dir'] is not None else [] for x in data['cls_names']]
        data[key]['cls_lfiles'] = []
        data[key]['cls_files'] = []
        for cls_i in range(len(data['cls_names'])):
            lfiles = [os.path.join(data['cls_names'][cls_i], x) for x in data[key]['cls_fnames'][cls_i]]
            data[key]['cls_lfiles'].append(lfiles)
            data[key]['cls_files'].append([os.path.join(data[key]['dir'], x) for x in lfiles])    
    data = {'train':{}, 'validate':{}}
    data['dir'] = data_dir
    for key, dirmatch in [ ('train', 'train'), ('validate', 'valid') ]:
        matchnames = [x for x in f_dirnames(data_dir) if x.lower().startswith(dirmatch)]
        if len(matchnames):
            dirname = matchnames[0]
            data[key]['dirname'] = dirname; data[key]['dir'] = os.path.join(data_dir, dirname)
        else:
            data[key]['dirname'] = None; data[key]['dir'] = None;
    data['cls_names'] = [x for x in sorted(f_dirnames(data['train']['dir'])) if excl_cls_names is None or x not in excl_cls_names]
    data['n_classes'] = len(data['cls_names'])
    for key in ['train', 'validate']:
        fill_cls_files(data, key)
    return data

def f_summarize_data(data):
    return ' '.join(['[{}: {}, {}]'.format(data['cls_names'][i], len(data['train']['cls_files'][i]), len(data['validate']['cls_files'][i])) for i in range(data['n_classes'])])
    
def f_rand_data_file(data, key):
    rnd_clsi = random.randint(0, data['n_classes']-1)
    rnd_imgi = random.randint(0, len( data[key]['cls_files'][rnd_clsi])-1)
    return (rnd_clsi, rnd_imgi, data[key]['cls_files'][rnd_clsi][rnd_imgi])

f_rand_train_file = lambda data: f_rand_data_file(data, 'train')
f_rand_validate_file = lambda data: f_rand_data_file(data, 'validate')

g_data = f_data_from_dir(g_data_base_dir)
print 'Data summary is:', f_summarize_data(g_data)


# In[ ]:

### Environment init
import os
os.environ["THEANO_FLAGS"] = "mode=FAST_RUN,device=gpu,floatX=float32,optimizer_including=cudnn"
#os.environ["THEANO_FLAGS"] = "mode=FAST_RUN,device=gpu,floatX=float32"


# In[ ]:

### Keras init
import keras

from tensorflow.python.client import device_lib

def f_keras_devices():
    return device_lib.list_local_devices()
    print local_device_protos
    return [x.name for x in local_device_protos if x.device_type == 'GPU']
def f_keras_gpus():
    local_device_protos = device_lib.list_local_devices()
    return [x.name for x in local_device_protos if x.device_type == 'GPU']
print 'Keras backend: [{}]'.format(keras.backend.backend())
print 'Keras devices:'; print '------'; print f_keras_devices(); print '------'; 
#print os.environ['CUDA_VISIBLE_DEVICES']
# devices and theano: http://stackoverflow.com/questions/30184994/how-can-i-change-device-used-of-theano
# devices and tf: http://learningtensorflow.com/lesson10/, http://stackoverflow.com/questions/34375871/overriding-device-scope-in-tensorflow


# In[ ]:

def f_theano_device_test():
    # Start gpu_test.py
    # From http://deeplearning.net/software/theano/tutorial/using_gpu.html#using-gpu
    from theano import function, config, shared, sandbox
    import theano.tensor as T
    import numpy
    import time

    vlen = 10 * 30 * 768  # 10 x #cores x # threads per core
    iters = 1000

    rng = numpy.random.RandomState(22)
    x = shared(numpy.asarray(rng.rand(vlen), config.floatX))
    f = function([], T.exp(x))
    print(f.maker.fgraph.toposort())
    t0 = time.time()
    for i in xrange(iters):
        r = f()
    t1 = time.time()
    print("Looping %d times took %f seconds" % (iters, t1 - t0))
    print("Result is %s" % (r,))
    if numpy.any([isinstance(x.op, T.Elemwise) for x in f.maker.fgraph.toposort()]):
        print('Used the cpu')
    else:
        print('Used the gpu')
    # End gpu_test.py

if keras.backend.backend() == 'theano':
    f_theano_device_test()


# In[ ]:

### VGG16
import os, sys
import h5py
import numpy as np
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Convolution2D, MaxPooling2D, ZeroPadding2D
from keras.layers import Activation, Dropout, Flatten, Dense
import tensorflow

# vgg pre-trained
g_vgg16_imagenet_th_weights_file = f_data('../../shared/vgg16_weights_ILSVRC-2014.h5')
if False and (keras.backend.backend() != 'theano' or keras.backend.image_dim_ordering() != 'th'):
    assert False, 'So far, we failed to convert the .h5 file to work properly with a tensorflow backend. TODO!!!'
    
def f_vgg16_resolve_img_sz(img_sz):
    if input in ['ILSVRC-2014', 'imagenet']:
        return (img_sz, img_sz)
    return tuple(f_explode(img_sz, 2))
def f_vgg16_resolve_weights(weights):
    if weights in ['ILSVRC-2014', 'imagenet']:
        return g_vgg16_imagenet_th_weights_file
    return weights

def f_make_vgg16_model(img_sz=None, bottleneck=None, top=None, bottleneck_weights=None, bottleneck_freeze=None, weights=None, verbose=True):
    model = None
    if img_sz is not None:
        model = Sequential()
        model.add(ZeroPadding2D((1, 1), input_shape=(3, f_vgg16_resolve_img_sz(img_sz)[0], f_vgg16_resolve_img_sz(img_sz)[1])))
    if bottleneck is not None:
        if model is None:
            model = Sequential()
        #
        model.add(Convolution2D(64, 3, 3, activation='relu', name='conv1_1'))
        model.add(ZeroPadding2D((1, 1)))
        model.add(Convolution2D(64, 3, 3, activation='relu', name='conv1_2'))
        model.add(MaxPooling2D((2, 2), strides=(2, 2)))
        #
        model.add(ZeroPadding2D((1, 1)))
        model.add(Convolution2D(128, 3, 3, activation='relu', name='conv2_1'))
        model.add(ZeroPadding2D((1, 1)))
        model.add(Convolution2D(128, 3, 3, activation='relu', name='conv2_2'))
        model.add(MaxPooling2D((2, 2), strides=(2, 2)))
        #
        model.add(ZeroPadding2D((1, 1)))
        model.add(Convolution2D(256, 3, 3, activation='relu', name='conv3_1'))
        model.add(ZeroPadding2D((1, 1)))
        model.add(Convolution2D(256, 3, 3, activation='relu', name='conv3_2'))
        model.add(ZeroPadding2D((1, 1)))
        model.add(Convolution2D(256, 3, 3, activation='relu', name='conv3_3'))
        model.add(MaxPooling2D((2, 2), strides=(2, 2)))
        #
        model.add(ZeroPadding2D((1, 1)))
        model.add(Convolution2D(512, 3, 3, activation='relu', name='conv4_1'))
        model.add(ZeroPadding2D((1, 1)))
        model.add(Convolution2D(512, 3, 3, activation='relu', name='conv4_2'))
        model.add(ZeroPadding2D((1, 1)))
        model.add(Convolution2D(512, 3, 3, activation='relu', name='conv4_3'))
        model.add(MaxPooling2D((2, 2), strides=(2, 2)))
        #
        model.add(ZeroPadding2D((1, 1)))
        model.add(Convolution2D(512, 3, 3, activation='relu', name='conv5_1'))
        model.add(ZeroPadding2D((1, 1)))
        model.add(Convolution2D(512, 3, 3, activation='relu', name='conv5_2'))
        model.add(ZeroPadding2D((1, 1)))
        model.add(Convolution2D(512, 3, 3, activation='relu', name='conv5_3'))
        model.add(MaxPooling2D((2, 2), strides=(2, 2)))
        if bottleneck_weights is not None:
            if weights is not None:
                return None
            bottleneck_weights = f_vgg16_resolve_weights(bottleneck_weights)
            if verbose:
                print 'loading vgg16 bottleneck from [{}] ..'.format(f_no_base(bottleneck_weights)),
            f = h5py.File(bottleneck_weights)
            try:
                for k in range(f.attrs['nb_layers']):
                    if k >= len(model.layers):
                        break
                    g = f['layer_{}'.format(k)]
                    #print g, g.attrs['nb_params']
                    weights_ = [g['param_{}'.format(p)] for p in range(g.attrs['nb_params'])]
                    #print weights_
                    model.layers[k].set_weights(weights_)
            finally:
                f.close()
            if verbose:
                print '.'
        if bottleneck_freeze:
            for layer in model.layers:
                layer.trainable = False
    if top is not None:
        model_top = model
        model_top.add(Flatten())
        ''' If you want to specify an input shape manually, e.g when creating a a standalone top, 
          this will depend on the image size: 
            - http://stackoverflow.com/questions/38972156/why-are-inputs-for-convolutional-neural-networks-always-squared-images
            - https://blog.keras.io/category/demo.html
            - https://www.kaggle.com/c/state-farm-distracted-driver-detection/discussion/20747
            
         # model_top.add(Flatten(input_shape=(512, 7, 7) if top_input is None else top_input))
         If there is a mismatch, it might possibly only be caught late, within theano code.
         'The shape of the input to "Flatten" is not fully defined': https://github.com/fchollet/keras/issues/1592
         usually an image-ordering issue, that can be solved by adjusting input: https://github.com/fchollet/keras/blob/master/keras/applications/imagenet_utils.py
        '''
        if top in ['ILSVRC-2014', 'imagenet']:
            model_top.add(Dense(4096, activation='relu', name='fc1'))
            model_top.add(Dropout(0.5))
            model_top.add(Dense(4096, activation='relu', name='fc2'))
            model_top.add(Dropout(0.5))
            model_top.add(Dense(1000, activation='softmax', name='predictions'))
        elif type(top) == int:
            model_top.add(Dense(256, activation='relu'))
            model_top.add(Dropout(0.5))
            #model_top.add(Dense(top if top > 2 else 1, activation='softmax')) # always use 2 and 'categorical' instead of 'binary' for now
            model_top.add(Dense(top, activation='softmax'))
        elif hasattr(top, '__iter__'):
            n_cls, top_type, layer_sz = top[0], f_aget(top, 1, ''), f_aget(top, 2, 256)
            if top_type == 'double':
                model_top.add(Dense(layer_sz, activation='relu'))
                model_top.add(Dropout(0.5))
                model_top.add(Dense(layer_sz, activation='relu'))
                model_top.add(Dropout(0.5))
            else:
                model_top.add(Dense(layer_sz, activation='relu'))
                model_top.add(Dropout(0.5))
            model_top.add(Dense(n_cls, activation='softmax'))
        else:
            return None
    if weights is not None and model is not None:
        weights = f_vgg16_resolve_weights(weights)
        if verbose:
            print 'loading vgg16 from [{}] ..'.format(f_no_base(weights)),
        model.load_weights(weights)
        if verbose:
            print '.'
    return model


def f_vgg16_retop_and_tune(data, img_sz, n_tv_samples = None, n_epochs = None, batch_sz = None, 
                           bneck_weights = None, weights = None, hist_file = None, top_opts=None, augment_kwargs = {}):
    model = f_make_vgg16_model(img_sz, bottleneck=True, bottleneck_weights=bneck_weights, 
                               bottleneck_freeze=True, top=data['n_classes'] if top_opts is None else [data['n_classes']]+top_opts, weights = weights)
    if n_tv_samples is not None:
        model.compile(optimizer='rmsprop', metrics=['accuracy'], loss='categorical_crossentropy')
        n_tv_samples = [max(int(x/batch_sz)*batch_sz, batch_sz) for x in f_explode(n_tv_samples, 2)]
        n_samples = {'train':n_tv_samples[0], 'validate':n_tv_samples[1]}
        generators = {}
        augment_kwargs = f_explode(augment_kwargs, 2)
        for ki, key in enumerate(['train', 'validate']):
            datagen = ImageDataGenerator(rescale=1./255, **augment_kwargs[ki])
            generators[key] = datagen.flow_from_directory(data[key]['dir'],
                            target_size=f_vgg16_resolve_img_sz(img_sz),
                            batch_size=batch_sz, class_mode='categorical',
                            classes = data['cls_names']) if data[key]['dir'] is not None else None
        timer = f_start_timer()
        fit_history = model.fit_generator(generators['train'], samples_per_epoch=n_samples['train'],
                            validation_data=generators['validate'], nb_val_samples=n_samples['validate'],
                            nb_epoch=n_epochs)
        if hist_file is not None:
            with open(hist_file, "w") as ft:
                print 'training history written to [{}]'.format(f_no_base(hist_file))
                ft.write(str(fit_history.history)) 
        f_end_timer(timer)
    return model

def f_vgg16_make_adapted(data, img_sz, weights_file, hist_file, refine, no_cache, 
                         n_tv_samples, n_epochs, batch_sz, 
                         top_opts=None, augment_kwargs = {}):
    def go_retop_and_tune(bneck_weights = None, weights=None):
        return f_vgg16_retop_and_tune(data, img_sz, n_tv_samples, n_epochs, batch_sz, 
                                        bneck_weights = bneck_weights, weights=weights, 
                                        hist_file = hist_file, 
                                        top_opts=top_opts, augment_kwargs=augment_kwargs)
    model = None
    if file_size(weights_file).startswith('0') or no_cache:
        print 'caching [{}] ..'.format(f_no_base(weights_file), file_size(weights_file)),
        model = go_retop_and_tune(bneck_weights = g_vgg16_imagenet_th_weights_file)
        model.save_weights(weights_file)
        print '{}.'.format(file_size(weights_file))
    elif refine:
        print 'refining [{}] ..'.format(f_no_base(weights_file), file_size(weights_file)),
        model = go_retop_and_tune(weights = weights_file)
        model.save_weights(weights_file)
        print '{}.'.format(file_size(weights_file))
    else:
        model = f_vgg16_retop_and_tune(data, img_sz, weights = weights_file, top_opts=top_opts)
        print 'in cache [{}] : {}.'.format(f_no_base(weights_file), file_size(weights_file))
    return model


# In[ ]:

#g_img_sz = 80
#f_vgg16_retop_and_tune(g_data, img_sz = g_img_sz, n_tv_samples=(200, 10), n_epochs=25, batch_sz=32)
# datasci, elpased [310.68 sec.] ,img_sz = 80, n_tv_samples=(200, 10), n_epochs=25, batch_sz=32)
# p2, elpased [340 sec.] ,img_sz = 80, n_tv_samples=(200, 10), n_epochs=25, batch_sz=32)

if True:
    g_img_sz = 100
    g_kglfish_weights_file = f_data('vgg16_kglfish_weights.h5')
    g_kglfish_hist_file = f_data('vgg16_kglfish_weights_hist.txt')
    augment_kwargs = {'rotation_range':40, 'width_shift_range':0.2, 'height_shift_range':0.2,
                          'shear_range':0.2, 'zoom_range':0.2, 'horizontal_flip':True, 'fill_mode':'nearest'}
    g_epochs = g_arg_epochs
    g_batch_sz = g_arg_batch
    g_refine = g_arg_refine
    g_no_cache = g_arg_no_cache
    model = f_vgg16_make_adapted(g_data, img_sz = g_img_sz, 
                         weights_file = g_kglfish_weights_file, 
                         hist_file =g_kglfish_hist_file,
                         refine = g_refine, no_cache = g_no_cache, n_tv_samples=(8*5000, 0), n_epochs=g_epochs, batch_sz=g_batch_sz,
                                top_opts=['double', 512], augment_kwargs=augment_kwargs)


# In[ ]:

import keras.preprocessing.image
import numpy

img_width,img_height=g_img_sz,g_img_sz
def f_classif_img(model, img_path, thumb=0):
    img_path = f_data(img_path)
    if (thumb > 0):
        IP.display.display(IP.display.Image(img_path, width=thumb))
    img = keras.preprocessing.image.load_img(img_path, target_size=(img_width, img_height))
    x = keras.preprocessing.image.img_to_array(img)
    x = numpy.expand_dims(x, axis=0)
    preds = model.predict(x)
    return preds[0]

def f_classif_print(model, img_path, thumb=0):
    preds = f_classif_img(model, img_path, thumb)
    pred_cls = g_data['cls_names'][ sorted([(xi, float(preds[xi])) for xi in range(len(preds))], key = lambda x: x[1])[-1][0] ]
    pred_str = '({})'.format(' '.join(['{:.2f}'.format(x) for x in preds]))
    return ' -> {} {}'.format(pred_cls, pred_str)


# In[ ]:

tuned_model = model
for i in range(10):
    test_file = f_rand_train_file(g_data)[2]; print f_no_base(test_file), f_classif_print(tuned_model, test_file);
for i in range(10):
    test_file = f_rand_validate_file(g_data)[2]; print f_no_base(test_file), f_classif_print(tuned_model, test_file);


# In[ ]:




# In[ ]:



