# Assumes following default file structure:
#
#     ~/kaggle/
#         train/
#             ALB/
#                 img_00003.jpg
#                 img_00010.jpg
#                 ...
#             BET/
#             ...
#             YFT/
#             annotations/
#                 ALB.json
#                 BET.json
#                 ...
#                 YFT.json
#         test/
#             img_01234.jpg
# TODO:
#     - Does not yet automatically find correct bounding box for final answers


import os, json, sys, random, shutil
import numpy as np
import PIL
from PIL import Image #pip install pillow
from keras.datasets import cifar10
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten, Convolution2D
from keras.layers.pooling import MaxPooling2D
from keras.layers.normalization import BatchNormalization
from keras.utils import np_utils
from keras.preprocessing.image import ImageDataGenerator

datasets = ['ALB','BET','DOL','LAG','NoF','OTHER','SHARK','YFT']
#datasets = ['ALB','BET','DOL','LAG','OTHER','SHARK','YFT']
path_train = os.path.expanduser('~/kaggle/train')
path_test = os.path.expanduser('~/kaggle/test_stg1')
path_predictions = os.path.expanduser('~/kaggle/predictions.csv')
path_annotations = os.path.expanduser('~/kaggle/train/annotations')
path_patches = os.path.expanduser('~/kaggle/patches')
make_square_images = True
resize_patches = True
resized_patch_size = 50
channels = 10
hidden1 = 2000
hidden2 = 2000
NoF_patches_per_file = 4 
colors = 3
output_dimension = len(datasets)
skipNoFish=True # False
sanity_check=False
generate_samples=True
seed=123
epochs=500 # 500
batch_size = 32

def loadPatches(dataset, class_id):
	print('')
	print('===============================================================')
	print('Loading patches for '+dataset+' dataset:')
	path_in = path_patches+'/'+dataset
	files = [f for f in os.listdir(path_in) if os.path.isfile(path_in+'/'+f)]
	files = sorted(files)
	patches = []
	targets = []
	patches_test = []
	targets_test = []
	for i, file in enumerate(files):
		if i%100 == 0:
			sys.stdout.write('.')
			sys.stdout.flush()
		img = np.asarray(Image.open(path_in+'/'+file))
		target = [0] * output_dimension
		target[class_id] = 1.0
		if random.random() < 0.8:
			patches.append(img)
			targets.append(target)
		else:
			patches_test.append(img)
			targets_test.append(target)
	return patches, targets, patches_test, targets_test

def generatePatches(dataset, hasFish):
	print('')
	print('===============================================================')
	print('Generating patches for '+dataset+' dataset:')
	if sanity_check:
		print('Sanity check: using upper left corners')
	path_in = path_train+'/'+dataset
	files = [f for f in os.listdir(path_in) if os.path.isfile(path_in+'/'+f)]
	print('\t' + str(len(files)) + ' total files')
	path_out = path_patches+'/'+dataset
	if not os.path.exists(path_out):
		print('\tcreating directory ' + path_out)
		os.makedirs(path_out)

	sys.stdout.write('\tgenerating patches')

	if hasFish:
		with open(path_annotations + '/' + dataset + '.json') as f:
			annotations = json.load(f)

	files = sorted(files)

	tot = 0
	avg_max_dimension = 0
	max_dimension = 0
	warnings = []
	for i, file in enumerate(files):
		if i%100 == 0:
			sys.stdout.write('.')
			sys.stdout.flush()
		img = np.asarray(Image.open(path_in+'/'+file))
		if not hasFish:
			for num in range(NoF_patches_per_file):

				height = resized_patch_size
				width = resized_patch_size
				y = random.randint(0, img.shape[0] - 1 - height)
				x = random.randint(0, img.shape[1] - 1 - width)

				if sanity_check:
					y = 0
					x = 0
					height = resized_patch_size
					width = resized_patch_size

				patch = img[y:y+height, x:x+width]

				save_path = path_out+'/'+file.replace('.jpg','')+'_'+str(num)+'.jpg'
				img2 = Image.fromarray(patch)
				img2.save(save_path)
				tot += 1
		else:
			for item in annotations:
				if item['filename'] == dataset + '/'+file:
					if len(item['annotations']) == 0:
						warnings.append('WARNING: missing annotations for '+file)
					for num, annotation in enumerate(item['annotations']):
						height = max(0, int(round(annotation['height'])))
						width = max(0, int(round(annotation['width'])))
						x = max(0, int(round(annotation['x'])))
						y = max(0, int(round(annotation['y'])))

						if sanity_check:
							x = 0
							y = 0
							height = resized_patch_size
							width = resized_patch_size

						if make_square_images:
							# Extend borders to always be square, and fish should be precisely in center?
							if height > width:
								diff = height - width
								x -= diff/2
								width = height
							elif width > height:
								diff = width - height
								y -= diff/2
								height = width

							# make sure it doesn't violate bounds
							if x < 0:
								x = 0
							elif width + x >= img.shape[1]:
								x = img.shape[1] - width - 1
							if y < 0:
								y = 0
							elif height + y >= img.shape[0]:
								y = img.shape[0] - height - 1

						patch = img[y:y+height, x:x+width]

						save_path = path_out+'/'+file.replace('.jpg','')+'_'+str(num)+'.jpg'
						img2 = Image.fromarray(patch)
						if resize_patches:
							img2 = img2.resize((resized_patch_size,resized_patch_size), PIL.Image.ANTIALIAS)
						img2.save(save_path)
						tot += 1
						avg_max_dimension += float(max(height, width))
						max_dimension = max(max_dimension, max(height, width))
	assert tot > 0
	print('done. ('+str(tot)+' total patches)')
	if hasFish:
		print('\tAvg max dimension = ' + str(avg_max_dimension/tot))
		print('\tMax dimension     = ' + str(max_dimension))
	for warning in warnings:
		print(warning)

def sanityTest(model):
	print('')
	print('-----------------------------------------')
	print('SANITY TEST')
	files = [f for f in os.listdir(path_test)]
	files = sorted(files)
	patches = []
	for i, file in enumerate(files):
		if i%100 == 0:
			sys.stdout.write('.')
			sys.stdout.flush()
		img = np.asarray(Image.open(path_test+'/'+file))
		patch = img[0:resized_patch_size, 0:resized_patch_size]
		patches.append(patch)
	print('done loading patches')
	X = patches
	X = np.array(X)
	X = X.astype('float32')
	X /= 255.0

	Y = model.predict(X, verbose=1)

	print('Predictions:')
	print(Y)

	print(Y[0])
	print('sum =' + str(np.sum(Y[0])))

	assert len(Y) == len(files)

	csv = 'image,ALB,BET,DOL,LAG,NoF,OTHER,SHARK,YFT\n'
	laplace = 0.05
	for i,f in enumerate(files):
		csv += f
		for j in range(8):
			p = Y[i][j]
			p_ = (p + laplace)/(1 + laplace*8)
			csv += ',' + str(p_)
		csv += '\n'

	with open(path_predictions,'wb') as f:
		f.write(csv)

	print('Done!')

def main():

	random.seed(seed)

	if generate_samples:
		shutil.rmtree(path_patches)
		for dataset in datasets:
			if dataset == 'NoF':
				generatePatches(dataset, False)
			else:
				generatePatches(dataset, True)

		print('done generating patches.')

	X = []
	Y = []
	X_test = []
	Y_test = []
	for i, dataset in enumerate(datasets):
		patches, classes, patches_test, classes_test = loadPatches(dataset, i)
		X.extend(patches)
		Y.extend(classes)
		X_test.extend(patches_test)
		Y_test.extend(classes_test)

	X = np.array(X)
	X = X.astype('float32')
	X /= 255.0
	Y = np.array(Y)

	X_test = np.array(X_test)
	X_test = X_test.astype('float32')
	X_test /= 255.0
	Y_test = np.array(Y_test)

	assert len(X) == len(Y)
	assert len(X_test) == len(Y_test)

	print('')
	print('')
	print('done generating data')
	print('X.shape = ' + str(X.shape))
	print('Y.shape = ' + str(Y.shape))
	print('X_test.shape = ' + str(X_test.shape))
	print('Y_test.shape = ' + str(Y_test.shape))

	model = Sequential()
	model.add(Convolution2D(channels, 3, 3, input_shape=(X.shape[1], X.shape[2], X.shape[3])))
	model.add(MaxPooling2D())

	model.add(Convolution2D(channels, 3, 3))
	model.add(MaxPooling2D())

	model.add(Flatten())

	model.add(Dense(hidden1))
	model.add(Activation('relu'))

	model.add(Dense(hidden2))
	model.add(Activation('relu'))

	model.add(Dense(Y.shape[1]))
	model.add(Activation('softmax'))

	model.compile(loss='categorical_crossentropy', optimizer='adadelta', metrics=['accuracy'])

	datagen = ImageDataGenerator(
		#rotation_range=30,
		width_shift_range=0.1,
		height_shift_range=0.1,
		horizontal_flip=True)

	datagen.fit(X)

	model.fit_generator(
		datagen.flow(X, Y, batch_size=batch_size),
		samples_per_epoch=1000,
		validation_data=(X_test, Y_test),
		nb_epoch=epochs)

	if sanity_check:	
		sanityTest(model)

if __name__ == '__main__':
	main()
