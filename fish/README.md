# Forum Watch
 1. [Using Keras+TensorFlow to solve NCFM-Leadboard Top 5%](https://www.kaggle.com/c/the-nature-conservancy-fisheries-monitoring/forums/t/26202/using-keras-tensorflow-to-solve-ncfm-leadboard-top-5)

# Fish Detection Literature
 1. [An Implementation of Tracking and Detecting Fish from Videos Using Adaptive Gaussian Mixture Model](http://www.ijarcce.com/upload/2016/september-16/IJARCCE%2050.pdf)
 2. [Automatic Fish Segmentation and Recognition for Trawl-based Cameras](https://books.google.de/books?id=FtrHCgAAQBAJ&pg=PA87&lpg=PA87&dq=detecting+fish+using+histograms&source=bl&ots=yNaiwYD8RC&sig=73ksPwG18cNfIbiiQc1ZtHYwBYw&hl=en&sa=X&ved=0ahUKEwj5046J0-rQAhWEaxQKHbiWBpcQ6AEISDAH#v=onepage&q=detecting%20fish%20using%20histograms&f=false) 
 3. [Fish Shape Recognition –An Approach using Histogram, Edge Detection and Hough Transform](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.671.8569&rep=rep1&type=pdf)