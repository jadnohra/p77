# Goals
 1. The main goal is experience and jumps into topics one would not usually jump into, even if shallow.
 2. To maximize the benefits, it would be nice to write a short summary of each session one did, even if a single line, especially when it is a task that someone will work on by themselves. This stops being useful once all parties involved know the same things.
  - [log/Jad](./log/jad.md)
  - [log/Tom](./log/tom.md)
 3. Code
  - [code/Jad](./code/jad)
  - [code/Tom](./code/tom)

# Competitions
 1. [Kaggle/fish](https://www.kaggle.com/t/428931/p77), [Repo](/fish)
    - [Sharing forum thread](https://www.kaggle.com/c/the-nature-conservancy-fisheries-monitoring/forums/t/25428/official-pre-trained-model-and-data-thread). Summary is that one must share any external data on the thread (it is not clear to me 'when' must one share it), however, one does not HAVE to share manual labelings of shared data. One must be careful with licenses of external data.
    - I asked about the 'when' on the forum, and the answer is that there is no written rule, and that 'sharing at the last minute of the competition' is considered 'unfair', so it is allowed.
    - We ended up not managing to submit any entry. Here is the presentation of the [winning team](https://github.com/tnc-ca-geo/fishface-tuna/blob/master/1_TROLL/TowardsRobustOptimalLearningofLearning-Slides.pdf)

# Kaggle General Tips
 1. [Unofficial kaggle command line tool (e.g download competition data without browser since wget fails)](https://github.com/floydwch/kaggle-cli)
 2. [Speed is NOT an issue!](https://www.kaggle.com/c/axa-driver-telematics-analysis/forums/t/11398/time-of-execution?forumMessageId=62133#post62133)

# General References
 1. [Kaggle Projects on Github](https://github.com/search?utf8=✓&q=kaggle)
    - https://github.com/tdeboissiere/Kaggle
    - https://github.com/SudalaiRajkumar/Kaggle
 2. Machine vision
    - [Local Invariant Feature Detectors: A Survey](http://www.eng.auburn.edu/~roppeth/courses/7970%202015A%20AdvMobRob%20sp15/literature/%5B2008%5D%20Local%20Invariant%20Feature%20Detectors-%20A%20Survey.pdf)     
 3. Convolutional nets
    - [CS231n class notes](http://cs231n.github.io/convolutional-networks/)
 4. History
    - [A 'Brief' History of Neural Nets and Deep Learning](http://www.andreykurenkov.com/writing/a-brief-history-of-neural-nets-and-deep-learning/)
 5. [ML+reddit](https://www.reddit.com/r/MachineLearning/)
    - [What happened to dropout](https://www.reddit.com/r/MachineLearning/comments/5l3f1c/d_what_happened_to_dropout/?)
 6. [Arxiv sanity](http://www.arxiv-sanity.com)
 7. [Great practical tips that might take time to find by oneself](http://lamda.nju.edu.cn/weixs/project/CNNTricks/CNNTricks.html)

# Introductions
 - [Jupyter Notebook Tutorial: The Definitive Guide](https://www.datacamp.com/community/tutorials/tutorial-jupyter-notebook#gs.9WiqMWA) (jupyter, history, docker)
 - [Funny discussion about NN vs SNM in 2012](http://stats.stackexchange.com/questions/30042/neural-networks-vs-support-vector-machines-are-the-second-definitely-superior)

# Documented Attempts and Failures
 1. Machine Vision
    - [Right Whale Recognition 1](https://andraszsom.wordpress.com/2016/01/11/right-whale-recognition-kaggle/) (template-matching, SVM, PCA, face)
 
# Computer vision papers
 1. Recurrent attentional mechanisms
    - [Recurrent Models of Visual Attention](https://papers.nips.cc/paper/5542-recurrent-models-of-visual-attention.pdf)
    - [Show, Attend and Tell: Neural Image Caption Generation with Visual Attention](https://arxiv.org/pdf/1502.03044.pdf)
 2. Object detection
    - [You Only Look Once (YOLO): Unified, Real-Time Object Detection](http://www.gitxiv.com/posts/wh64sGMfwegjHyHFq/you-only-look-once-unified-real-time-object-detection)
    - [YOLO9000: Better, Faster, Stronger](https://arxiv.org/abs/1612.08242)
 3. Tips and Tricks
    - [Different Image Sizes](https://www.quora.com/How-are-images-of-different-shapes-handled-as-input-to-convolutional-networks) 
 4. Feedforward architectures (ResNet, etc.)
    - [Highway and Residual Networks learn Unrolled Iterative Estimation](https://arxiv.org/abs/1612.07771)

# Data augmentation
 1. Supplementary data sets (open source)
    - [Fish Dataset](https://wiki.qut.edu.au/display/cyphy/Fish+Dataset)
 2. [Keras image preprocessing](https://keras.io/preprocessing/image/)

# Segmentation
 1. Forget segmentation... 
    - [Edge Boxes: Locating Object Proposals from Edges](http://web.bii.a-star.edu.sg/~zhangxw/files/EdgeBoxes_ECCV2014.pdf), on [github](https://github.com/pdollar/edges)
    - [BING: Binarized Normed Gradients for Objectness Estimation at 300fps](http://mmcheng.net/bing/)
    - [Category Independent Object Proposals](http://vision.cs.uiuc.edu/proposals/)

# Feature Extraction
 1. [Feature Extraction: Modern Questions and Challenges](http://www.feature-extraction.org)
 2. Survey, classical vs. deep learning: [A Survey of Modern Questions and Challenges in Feature Extraction](http://www.jmlr.org/proceedings/papers/v44/storcheus2015survey.pdf)
    - Offers open questions up for research. 

# Shape and Texture
 1. [On Modelling Nonlinear Shape-and-Texture Appearance Manifolds](http://groups.csail.mit.edu/vision/vip/papers/nlam_cvpr05.pdf) (old)
 2. [Blobworld: a System for Region-Based Image Indexing and Retrieval](http://digitalassets.lib.berkeley.edu/techreports/ucb/text/CSD-99-1041.pdf), [Blobworld Texture Features](https://courses.cs.washington.edu/courses/cse455/05wi/notes/BlobworldTexture.pdf) 
             
        An approach to overcome the limitations of the global features is to segment the image in a 
        limited number of regions or segments, with each such region corresponding to a single object 
        or part thereof. The best known example of this approach is the blobworld system, pro- posed in [31], 
        which segments the image based on color and texture, then searches a database for images with similar 
        “image blobs.” An example based on texture segmentation is the wide baseline matching work described in [208]. 
        However, this raises a chicken-and-egg problem as image segmen- tation is a very challenging task in itself, 
        which in general requires a high-level understanding of the image content. For generic objects, color and 
        texture cues are insufficient to obtain meaningful segmentations.

# Learning with few examples
 1. [Deep Learning vs. Bayesian Hierarchical Modeling](http://stats.stackexchange.com/questions/60153/whats-the-difference-between-deep-learning-and-multilevel-hierarchical-modeli), [Google search](https://www.google.de/search?rls=en&q=hierarchical+bayesian+learning)

# Libraries
 1. [Keras](https://keras.io/)
 2. [MinPy](https://github.com/dmlc/minpy)
 3. [AutoGrad](https://github.com/HIPS/autograd)

# Plan Outline
 - Fast feedback
   - Get a basic system (or multiple?) in place before worrying about any kind of data augmentation. Added data will improve whatever we have, but we should be able to make some high level architectural decisions without it initially.
   - Perhaps start with downsampled, black & white versions of the original training set to speed up processing
   - Perhaps convolve an auto-encoder over the raw input to learn a much smaller version of the inputs
 - Preprocessing
   - Apply [whitening](https://www.tensorflow.org/api_docs/python/image/image_adjustments#per_image_standardization)
   - [Keras image preprocessing](https://keras.io/preprocessing/image/)
   - "So I'm thinking next step is to try to compress the heck out of images in the training set. Maybe simply reducing their size, or train an autoencoder over randomly selected and transformed patches (say 50x50 pixels, or something relatively small like that.) Compressing them should 1) speed up training, and 2) improve generalization, assuming we don't compress TOO much"

# Learning resources
 - [Udacity online course](https://www.udacity.com/course/deep-learning--ud730)

# AWS / GPU
 - [Quick Comparison of TensorFlow GPU Performance on AWS P2 and G2 Instances](http://www.bitfusion.io/2016/11/03/quick-comparison-of-tensorflow-gpu-performance-on-aws-p2-and-g2-instances/)
   - tl;dr - p2 is better.
 - Install notes on EC2 GPU instance:
   - $ sudo apt-get install python-pip python-dev build-essential
   - Install CUDA dependencies, described [here](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/g3doc/get_started/os_setup.md#optional-install-cuda-gpus-on-linux) Need both Cuda Toolkit and cuDNN v5.1
     - Cuda Toolkit
         - Download cuda-repo-ubuntu1604_8.0.44-1_amd64.deb from [here](https://developer.nvidia.com/cuda-downloads), equivalently run 
                - wget http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/cuda-repo-ubuntu1604_8.0.44-1_amd64.deb
         - $ sudo dpkg -i cuda-repo-ubuntu1604_8.0.44-1_amd64.deb
         - $ sudo apt-get update
         - $ sudo apt-get install cuda
     - cuDNN v5.1
         - Register as Nvidia dev, and download from [here](https://developer.nvidia.com/cudnn)
         - Uncompress and copy the cuDNN files into the toolkit directory. Assuming the toolkit is installed in /usr/local/cuda, run the following commands (edited to reflect the cuDNN version you downloaded):
         - $ tar xvzf cudnn-8.0-linux-x64-v5.1.tgz
         - $ sudo cp -P cuda/include/cudnn.h /usr/local/cuda/include/
         - $ sudo cp -P cuda/lib64/libcudnn* /usr/local/cuda/lib64/
         - $ sudo chmod a+r /usr/local/cuda/include/cudnn.h /usr/local/cuda/lib64/libcudnn*
   - May need to reboot after installing Cuda (I did to be on the safe side)
   - $ sudo pip install tensorflow-gpu
     - If you get a locale error, try: $ export LC_ALL=C 
   - $ sudo pip install keras
   - $ sudo pip install pillow

# AWS tips
  1. Transfer files between instances
    * ssh to the target machine 
    * copy (e.g nano and paste) the source machine's ssh key to ~/foo/xxx.pem.txt 
    * $ chmod 400 ~/foo/xxx.pem.txt
    * $ sudo apt-get install sshfs
    * $ mkdir -p ~/foo/mnt
    * $ sshfs -o ssh_command='ssh -i ~/foo/xxx.pem.txt' admin@ec2-52-59-201-109.eu-central-1.compute.amazonaws.com:/usr/admin ~/foo/mnt'
    * $ ls ~/foo/mnt
    * $ cp -r ??? ~/foo/mnt/???
    * When done $ fusermount -u ~/foo/mnt
  2. Simple terminal browser when needed (e.g download install files that need web interaction)
    * $ sudo apt-get install links2
    * $ links2 url
  3. Handle instances from a terminal
    * Start/Stop $ aws ec2 start-instances --instance-id ??? $ aws ec2 stop-instances --instance-id ???
    * List instances and their information $ aws ec2 describe-instance
    * Find PublicDns $ aws ec2 describe-instances | grep "ec2-.*\.com" -o | sort | uniq
    * Copy PublicDns (mac) $ aws ec2 describe-instances | grep "ec2-.*\.com" -o | sort | uniq | pbcopy
    * ssh to machine (ubuntu) $ ssh -i ~/foo/xxx.pem.txt ubuntu@ec2-52-59-205-92.eu-central-1.compute.amazonaws.com
    * ssh to machine (debian) $ ssh -i ~/foo/xxx.pem.txt admin@ec2-52-59-205-92.eu-central-1.compute.amazonaws.com
  4. The easiest albeit unsecure way to run Jupyter notebooks from AWS is to use run wihtou ssl (so http, not https). Running with SSL has issues on Safari not accepting the certificate and silently failing to connect to the kernel forever even though it can open it.
  5. If you installed jupyter only with python3 kernels and have some python2 notebooks, use this: https://ipython.readthedocs.io/en/latest/install/kernel_install.html 
  6. conda does not have a prebuilt tensorflow with GPU support. To make it work nevertheless, within the conda python (2.7) environment, do not install tensorflow and keras using conda, but using pip:
    * pip install tensorflow
    * pip install tensorflow-gpu
    * pip install keras
    * pip install pillow
    * To check the tensorflow devices simply look at the output of $python -c "from tensorflow.python.client import device_lib; print(device_lib.list_local_devices());"
  7. A finally good effort to make aws ec2 instance information accessible. Even includes information about availability of p2 instances in a simple way. [http://www.ec2instances.info](http://www.ec2instances.info) 
  8. Added some aws commands to my ['please' tool](https://github.com/jadnohra/please). For now, copying to clipboard only works on the mac, and the path for ssh keys is hard-coded.
  9. To download a file shared on google docs/drive from a linux console [try this](http://unix.stackexchange.com/questions/136371/how-to-download-a-folder-from-google-drive-using-terminal):
    * $ wget --save-cookies cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?id=0Bz7KyqmuGsilT0J5dmRCM0ROVHc&export=download' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/Code: \1\n/p'
    * run below with ????? replaced by the code printed after running the above 
    * $ wget --load-cookies cookies.txt 'https://docs.google.com/uc?id=0Bz7KyqmuGsilT0J5dmRCM0ROVHc&export=download&confirm=??????'
  10. If using the same key file 'ssh -i ...' for multiple instances, this error can occur when logging into to one:
    
        > @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        > @    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
        > @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        > IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
        > ...

    The [solution](http://stackoverflow.com/questions/20840012/ssh-remote-host-identification-has-changed) is to simply run
     * ssh-keygen -R $hostname$
     
  11. To preview images in the terminal with a lightweight utility:

        > $sudo apt-get install caca-utils$
        > $cacaview /PATH/TO/image.jpg$
        > $cacaview /PATH/TO/*.jpg$
        
  12. To get the public aws hostname from within a linux instance try

        > $ec2metadata  --public-hostname
        > $ec2metadata
    
  