import os,sys,copy
import math
import numpy as np
import matplotlib.pyplot as pyplot
pyplot.rcParams['keymap.quit'] = 'q,escape'
import matplotlib.image as mpimg
import scipy.ndimage

##############################################################
# Formulas from:
# Robustness of classifiers: from adversarial to random noise
# (Alhussein Fawzi, Seyed-Mohsen Moosavi-Dezfooli, Pascal Frossard)
# https://arxiv.org/abs/1608.08967
##############################################################

# (3)
def zeta1(m,delta):
	def comp1(m,delta):
		return math.log(1.0/delta) / m
	return 1.0/(1.0 + 2.0*math.sqrt(comp1(m,delta)) + 2.0*comp1(m,delta))

# (4)
def zeta2(m,delta):
	return 1.0/( max( (1.0/math.e)*math.pow(delta,2.0/m), 1.0-math.sqrt(2.0*(1.0-math.pow(delta,2.0/m))) )  )

# (12)
def curv_cond(m,d,delta,rk2):
	return ((0.2/zeta2(m,delta)) * ((1.0*m)/d)) / (rk2)

# 13
def bounds_rs2(m,d,L,r2,delta):
	def comp1(m,d,r2):
		return math.sqrt((1.0*d)/m)*r2
	return [ 0.875*math.sqrt(zeta1(m,delta))*comp1(m,d,r2), 1.45*math.sqrt(zeta2(m,delta))*comp1(m,d,r2), 1.0-4.0*(L+2)*delta ]

def bounds_rs2_Pmin(m,d,L,r2, Pmin):
	delta = (1.0-Pmin) / (4.0*(L+2))
	return bounds_rs2(m,d,L,r2,delta)[:-1]

def plot_bounds_rs2(d=256*256,L=2,r2=1.0,Pmin=0.5,m_d=0.1):
	xM = int(d*m_d)
	X = np.arange(1,xM,1); xW=1;
	Ym,YM = zip(*[bounds_rs2_Pmin(x,d,L,r2,Pmin) for x in X])

	if True:
		Ym = [y/r2 for y in Ym]; YM = [y/r2 for y in YM];

	X = [1.0*x/d for x in X]; xW = X[-1]-X[-2];
	ymm = min(Ym); ymM = max(Ym); yMm = min(YM); yMM = max(YM);
	#print X
	print [0, X[-1]+xW, max(0,ymm-(ymM-ymm)*0.05), (yMM+(yMM-yMm)*0.05)]
	if False:
		pyplot.axis([X[0]-xW, X[-1]+xW, max(0,ymm-(ymM-ymm)*0.05), (yMM+(yMM-yMm)*0.05)/10.0])
	else:
		pyplot.axis([X[0]-xW, X[-1]+xW, Ym[len(Ym)/2], YM[int(len(YM)*0.01)]])
	if False:
		pyplot.plot(X,Ym, linestyle='-', marker='+', color='#e6ac00', linewidth=3.0)
		pyplot.plot(X,YM, linestyle='-', marker='+', color='#ffdb4d', linewidth=3.0)
	else:

		#pyplot.bar(X,YM,xW)
		#pyplot.bar(X,Ym,xW,color='w')

		pyplot.plot(X,Ym, linestyle='-', color='#e6ac00', linewidth=3.0)
		pyplot.plot(X,YM, linestyle='-', color='#ffdb4d', linewidth=3.0)
		pyplot.fill_between(X,Ym,YM, color='#4db8ff')
	pyplot.show()

# http://stackoverflow.com/questions/5408276/sampling-uniformly-distributed-random-points-inside-a-spherical-volume
def uniform_n_sphere(n, cnt=1):
	v = np.random.uniform(-1.0,1.0,size=(n, cnt))
	vn = v / np.sqrt(np.sum(v**2, 0))
	return np.transpose(vn)

def clamp_rgb(x, mode='RGB'):
	if mode == 'RGB':
		return int(max(min(255,x),0))

def uniform_pert_rgb(w, h, cols, ampl, rgb_mode='RGB'):
	v = uniform_n_sphere(w*h*cols)[0]
	ar = np.reshape(v, (w,h,cols))
	ar3 = np.zeros((w,h,cols))
	for i in range(len(ar3)):
		for j in range(len(ar3[i])):
			ar3[i][j] = [clamp_rgb(np.ceil(x), rgb_mode) for x in ampl*ar[i][j]]
	return ar3

def rgb_friendly_add(img, pert):
	pimg = np.zeros((len(img), len(img[0]), 3))
	for i in range(len(img)):
		img_i = img[i]; pert_i = pert[i]; pimg_i = pimg[i];
		for j in range(len(img[i])):
			img_ij = img_i[j]; pert_ij = pert_i[j]; pimg_ij = pimg_i[j];
			for c in range(3):
				px = int(img_ij[c])+pert_ij[c]
				if px < 0 or px > 255:
					px = img_ij[c]-pert_ij[c]
				pimg_ij[c] = px
	return pimg

def rgb_abs_diff(img1, img2, rgb_type, rgbdiff_type, diff_mul=1):
	diff = np.abs(np.subtract(img1.astype(rgbdiff_type), img2.astype(rgbdiff_type))).astype(rgb_type)
	if float(diff_mul) != 1.0:
		diff = (diff * diff_mul).astype(rgb_type)
	return diff

def perturb_image(imgf='bug.png',ampl=1.0,r2=1.0,L=5,sm_=0,smr=0.01,Pmin=0.7,raw_pert=False,force_norm=False,diff_mul=1):
	print 'image: {}'.format(imgf)
	print 'random seed check: {}'.format(np.random.randint(100))
	rgb_mode = 'RGB'; rgb_type = np.uint8; rgbdiff_type = np.int32;
	img = scipy.ndimage.imread(imgf, mode=rgb_mode)
	w,h = np.size(img, 0), np.size(img, 1)
	pimg_r1 = np.asarray(img, dtype=rgb_type)
	pimg_rs = np.asarray(img, dtype=rgb_type)
	test_ampl = ampl*math.sqrt(w*h*3)

	dim = w*h*3; sm = int(smr*dim) if sm_ <= 0 else sm_;
	print 'm = {}'.format(sm)
	bounds_r1 = bounds_rs2_Pmin(1,w*h*3,L,r2, Pmin)
	avg_r1 = (bounds_r1[0]+bounds_r1[1])/2.0
	print 'random norm: {} - avg: {}'.format(bounds_r1, avg_r1)
	bounds_rs = bounds_rs2_Pmin(sm,w*h*3,5,r2, Pmin)
	avg_rs = (bounds_rs[0]+bounds_rs[1])/2.0
	print 'semi-random norms: {} - avg: {}'.format(bounds_rs, avg_rs)

	pert_add_func = np.add if raw_pert else rgb_friendly_add

	print 'Generating random perturbation...'
	perts_r1 = [uniform_pert_rgb(w,h,3, avg_r1, rgb_mode)]
	for pert in perts_r1:
		pert_ampl = np.linalg.norm(np.reshape(pert, (dim)))
		if force_norm:
			pert = pert * (avg_r1 / np.linalg.norm(np.reshape(pert, (dim))))
			pert_ampl = np.linalg.norm(np.reshape(pert, (dim)))
		print ' Perturbing (norm={})...'.format(pert_ampl)
		pimg_r1 = pert_add_func(pimg_r1, pert).astype(rgb_type)

	rs_ampls = uniform_n_sphere(sm)[0] * avg_rs
	print 'Generating {} semi-random perturbations...'.format(sm)
	#print 'Sem-random norms', rs_ampls
	perts_rs = [uniform_pert_rgb(w,h,3, x, rgb_mode) for x in rs_ampls]
	for pert_i in range(len(perts_rs)):
		pert = perts_rs[pert_i]
		pert_ampl = np.linalg.norm(np.reshape(pert, (dim)))
		if pert_ampl == 0:
			print ' Skipping zero perturbation {} of {}...'.format(pert_i+1, sm)
		else:
			if force_norm:
				pert = pert * (rs_ampls[pert_i] / np.linalg.norm(np.reshape(pert, (dim))))
				pert_ampl = np.linalg.norm(np.reshape(pert, (dim)))
			print ' Perturbing {} of {}... (norm = {})'.format(pert_i+1, sm, pert_ampl)
			pimg_rs = pert_add_func(pimg_rs, pert).astype(rgb_type)

	#pyplot.figure(1)
	ax = pyplot.subplot(321); pyplot.imshow(img); ax.set_title('Original ({} x {} x 3 = {})'.format(w,h,dim)); ax.axis('off');
	ax = pyplot.subplot(323); pyplot.imshow(pimg_r1); ax.set_title('Random (norm={:.0f})'.format(avg_r1)); ax.axis('off');
	ax = pyplot.subplot(324); pyplot.imshow(rgb_abs_diff(img, pimg_r1, rgb_type, rgbdiff_type)); ax.set_title('Random diff'); ax.axis('off');
	ax = pyplot.subplot(325); pyplot.imshow(pimg_rs); ax.set_title('{}-random (norm={:.0f})'.format(sm,avg_rs)); ax.axis('off');
	ax = pyplot.subplot(326); pyplot.imshow(rgb_abs_diff(img, pimg_rs, rgb_type, rgbdiff_type, diff_mul)); ax.set_title('{}-random diff (x {:.1f})'.format(sm, diff_mul)); ax.axis('off');
	pyplot.show()

test1 = plot_bounds_rs2
test2 = perturb_image

def get_arg(key, dflt = ''):
	return sys.argv[sys.argv.index(key)+1] if key in sys.argv else dflt

def printAndChoose(list, postindex = False, forceChoose = False):
	if (len(list) == 0): return []
	if (len(list) == 1 and forceChoose == False): return [0]
	for i in range(len(list)):
		#print_coli(gAltCols[i % len(gAltCols)])
		if postindex:
			print '. {} ({})'.format(list[i], i+1)
		else:
			print '{}. {}'.format(i+1, list[i])
	#print_col('default')
	print '>',
	input_str = raw_input()
	choices = []
	if ('-' in input_str):
		list = input_str.split('-')
		choices = range(int(list[0]), int(list[1])+1)
	elif (',' in input_str):
		choices = [int(x) for x in input_str.split(',')]
	else:
		if len(input_str):
			choices.append(int(input_str))
	choices = [i-1 for i in choices]
	return choices

if '-seed' in sys.argv:
	np.random.seed(int(get_arg('-seed', 0)))

if '-imgs' in sys.argv:
	files = []
	for file in os.listdir("./"):
		exts = ['.png','.jpg','.jpeg']
		if any([file.endswith(x) for x in exts]):
			files.append(file)
	choices = printAndChoose(files)
	if len(choices):
		sys.argv.append('-img'); sys.argv.append(files[choices[0]])

if '-test1' in sys.argv:
	test1()

if '-test2' in sys.argv:
	test2(imgf = get_arg('-img', 'dog1.jpg')
		,ampl = float(get_arg('-ampl', 10.0))
		,r2 = float(get_arg('-r2', 1.0))
		,L = float(get_arg('-L', 5))
		,sm_ = int(get_arg('-m', 2))
		,smr = float(get_arg('-mr', 0.01))
		,Pmin = float(get_arg('-Pmin', 0.7))
		,raw_pert = bool(get_arg('-raw_pert', False))
		,force_norm = '-force_norm' in sys.argv
		,diff_mul = float(get_arg('-diff_mul', 1.0))
		)

if '-test2_1' in sys.argv:
	test2(imgf = get_arg('-img', 'dog1.jpg'),r2 = 1.0,L = 5,sm_ = 2,Pmin = 0.95,force_norm = False)

if '-test2_2' in sys.argv:
	test2(imgf = get_arg('-img', 'dog1.jpg'),r2 = 1.0,L = 5,sm_ = 4,Pmin = 0.95,force_norm = True,diff_mul=2)

if '-test2_3' in sys.argv:
	test2(imgf = get_arg('-img', 'dog1.jpg'),r2 = 10.0,L = 5,sm_ = 3,Pmin = 0.95,force_norm = True,diff_mul=2)

if '-test2_4' in sys.argv:
	test2(imgf = get_arg('-img', 'dog1.jpg'),r2 = 10.0,L = 5,sm_ = 6,Pmin = 0.95,force_norm = True,diff_mul=2)